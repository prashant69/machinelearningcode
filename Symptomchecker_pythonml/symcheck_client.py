# Copyright 2015 gRPC authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""The Python implementation of the GRPC helloworld.Greeter client."""

from __future__ import print_function

import grpc

import symptomchcker_pb2
import symptomchecker_pb2_grpc


def run():
    channel = grpc.insecure_channel('localhost:50051')
    stub = symptomchecker_pb2_grpc.GreeterStub(channel)
    arr1= ['Virat','Bhuvi','Dhoni','Chahal']
    arr2= ['ranbir','ranveer','hrithik']
    features = [["fever","chills","decreased energy or fatigue","diarrhea","headache","loss of appetite","muscle aches","nausea or vomiting","runny or stuffy nose","sore throat",">101 degree F"],
            ["fever","chest pain or tightness","chills","cough","decreased energy or fatigue","headache","muscle aches","runny or stuffy nose","shortness of breath","sore throat","<101 degree F"],
            ["fever","headache","nausea or vomiting","rash","red spots at the back of the roof of the mouth","red swollen tonsils","sore throat","swollen glands (lymph nodes) in neck","white patches or pus on tonsils","<101 degree F"],
            ["fever","headache","nausea or vomiting","rash","red spots at the back of the roof of the mouth","red swollen tonsils","sore throat","swollen glands (lymph nodes) in neck","white patches or pus on tonsils","<101 degree F"],
            ["fever","decreased energy or fatigue","loss of appetite","night sweats","rash","red swollen tonsils","sore throat","swollen glands (lymph nodes) in neck","<101 degree F"],
            ["fever","cough","rapid heartbeat","rapid or difficult breathing","runny or stuffy nose","wheezing",">101 degree F"],
            ["fever","cough","rapid heartbeat","rapid or difficult breathing","runny or stuffy nose","wheezing","<101 degree F"],
            ["fever","decreased energy or fatigue","diarrhea","loss of appetite","rash,swollen glands (lymph nodes) in neck","<101 degree F"],
            ["fever","abdominal pain or cramping","diarrhea","loss of appetite","muscle aches","nausea or vomiting",">101 degree F"],
            ["fever","bloody stools","decreased energy or fatigue","nausea or vomiting",">101 degree F"],
            ["fever","abdominal pain or cramping","bloody or cloudy urine","frequent or painful urination",">101 degree F"],
            ["fever","ear pain or discharge","headache","<101 degree F"]]
			
    labels =["Influenza","Pneumonia","Strep throat","Tonsilittis","Mononucleosis","Bronchiolitis","Respiratory syncytial virus (RSV)","Roseola","Viral gastroenteritis (stomach flu)","Rotavirus","Urinary tract infection (UTI)","Ear infection (middle ear)"]			
	
    sdata = [['<101 degree F', '>101 degree F', 'abdominal pain or cramping',
       'bloody or cloudy urine', 'bloody stools',
       'chest pain or tightness', 'chills', 'cough',
       'decreased energy or fatigue', 'diarrhea', 'ear pain or discharge',
       'fever', 'frequent or painful urination', 'headache',
       'loss of appetite', 'muscle aches', 'nausea or vomiting',
       'night sweats', 'rapid heartbeat', 'rapid or difficult breathing',
       'rash', 'rash,swollen glands (lymph nodes) in neck',
       'red spots at the back of the roof of the mouth',
       'red swollen tonsils', 'runny or stuffy nose',
       'shortness of breath', 'sore throat',
       'swollen glands (lymph nodes) in neck', 'wheezing',
       'white patches or pus on tonsils'],
       ["fever","bloody stools","decreased energy or fatigue","nausea or vomiting",">101 degree F"]]
	   
	   
    feat=[]
    dt=[]
	
    ar = symptomchecker_pb2.ArrayRequest()	
  #  ft = symptomchecker_pb2.Pythonml()
	
    for i in range(len(features)): 
        feat.append([symptomchecker_pb2.Pythonml(fdata = features[i])])
		
    for j in range(len(feat)):  
        ar.feature.extend(feat[j])		
	
    for k in range(len(sdata)):
        dt.append([symptomchecker_pb2.Selected(pdata=sdata[k])])
		
    for l in range(len(dt)):
        ar.data.extend(dt[l])	
	
    print([ar.feature])	
    print(type(ar.feature))		    				
	
    req = symptomchecker_pb2.ArrayRequest(label=labels,feature=ar.feature,data=ar.data)
  
    response = stub.SendFeatureLabel(req)
  
    print("Greeter client received: " + response.predict)
    	  

if __name__ == '__main__':
  run()
