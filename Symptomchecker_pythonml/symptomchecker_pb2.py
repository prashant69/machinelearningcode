# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: symptomchecker.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='symptomchecker.proto',
  package='generated',
  syntax='proto3',
  serialized_pb=_b('\n\x14symptomchecker.proto\x12\tgenerated\"f\n\x0c\x41rrayRequest\x12\r\n\x05label\x18\x01 \x03(\t\x12$\n\x07\x66\x65\x61ture\x18\x02 \x03(\x0b\x32\x13.generated.Pythonml\x12!\n\x04\x64\x61ta\x18\x03 \x03(\x0b\x32\x13.generated.Selected\"\x19\n\x08Pythonml\x12\r\n\x05\x66\x64\x61ta\x18\x01 \x03(\t\"\x19\n\x08Selected\x12\r\n\x05pdata\x18\x01 \x03(\t\"\x1e\n\x0bStringReply\x12\x0f\n\x07predict\x18\x01 \x01(\t2P\n\x07Greeter\x12\x45\n\x10SendFeatureLabel\x12\x17.generated.ArrayRequest\x1a\x16.generated.StringReply\"\x00\x42\x34\n!com.telehealth365.proto.generatedB\x08SymProtoP\x01\xa2\x02\x02SPb\x06proto3')
)




_ARRAYREQUEST = _descriptor.Descriptor(
  name='ArrayRequest',
  full_name='generated.ArrayRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='label', full_name='generated.ArrayRequest.label', index=0,
      number=1, type=9, cpp_type=9, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='feature', full_name='generated.ArrayRequest.feature', index=1,
      number=2, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='data', full_name='generated.ArrayRequest.data', index=2,
      number=3, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=35,
  serialized_end=137,
)


_PYTHONML = _descriptor.Descriptor(
  name='Pythonml',
  full_name='generated.Pythonml',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='fdata', full_name='generated.Pythonml.fdata', index=0,
      number=1, type=9, cpp_type=9, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=139,
  serialized_end=164,
)


_SELECTED = _descriptor.Descriptor(
  name='Selected',
  full_name='generated.Selected',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='pdata', full_name='generated.Selected.pdata', index=0,
      number=1, type=9, cpp_type=9, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=166,
  serialized_end=191,
)


_STRINGREPLY = _descriptor.Descriptor(
  name='StringReply',
  full_name='generated.StringReply',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='predict', full_name='generated.StringReply.predict', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=193,
  serialized_end=223,
)

_ARRAYREQUEST.fields_by_name['feature'].message_type = _PYTHONML
_ARRAYREQUEST.fields_by_name['data'].message_type = _SELECTED
DESCRIPTOR.message_types_by_name['ArrayRequest'] = _ARRAYREQUEST
DESCRIPTOR.message_types_by_name['Pythonml'] = _PYTHONML
DESCRIPTOR.message_types_by_name['Selected'] = _SELECTED
DESCRIPTOR.message_types_by_name['StringReply'] = _STRINGREPLY
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

ArrayRequest = _reflection.GeneratedProtocolMessageType('ArrayRequest', (_message.Message,), dict(
  DESCRIPTOR = _ARRAYREQUEST,
  __module__ = 'symptomchecker_pb2'
  # @@protoc_insertion_point(class_scope:generated.ArrayRequest)
  ))
_sym_db.RegisterMessage(ArrayRequest)

Pythonml = _reflection.GeneratedProtocolMessageType('Pythonml', (_message.Message,), dict(
  DESCRIPTOR = _PYTHONML,
  __module__ = 'symptomchecker_pb2'
  # @@protoc_insertion_point(class_scope:generated.Pythonml)
  ))
_sym_db.RegisterMessage(Pythonml)

Selected = _reflection.GeneratedProtocolMessageType('Selected', (_message.Message,), dict(
  DESCRIPTOR = _SELECTED,
  __module__ = 'symptomchecker_pb2'
  # @@protoc_insertion_point(class_scope:generated.Selected)
  ))
_sym_db.RegisterMessage(Selected)

StringReply = _reflection.GeneratedProtocolMessageType('StringReply', (_message.Message,), dict(
  DESCRIPTOR = _STRINGREPLY,
  __module__ = 'symptomchecker_pb2'
  # @@protoc_insertion_point(class_scope:generated.StringReply)
  ))
_sym_db.RegisterMessage(StringReply)


DESCRIPTOR.has_options = True
DESCRIPTOR._options = _descriptor._ParseOptions(descriptor_pb2.FileOptions(), _b('\n!com.telehealth365.proto.generatedB\010SymProtoP\001\242\002\002SP'))

_GREETER = _descriptor.ServiceDescriptor(
  name='Greeter',
  full_name='generated.Greeter',
  file=DESCRIPTOR,
  index=0,
  options=None,
  serialized_start=225,
  serialized_end=305,
  methods=[
  _descriptor.MethodDescriptor(
    name='SendFeatureLabel',
    full_name='generated.Greeter.SendFeatureLabel',
    index=0,
    containing_service=None,
    input_type=_ARRAYREQUEST,
    output_type=_STRINGREPLY,
    options=None,
  ),
])
_sym_db.RegisterServiceDescriptor(_GREETER)

DESCRIPTOR.services_by_name['Greeter'] = _GREETER

# @@protoc_insertion_point(module_scope)
