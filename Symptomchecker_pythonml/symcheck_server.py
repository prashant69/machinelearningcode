# Copyright 2015 gRPC authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""The Python implementation of the GRPC helloworld.Greeter server."""

from concurrent import futures
import time

import grpc
from sklearn.naive_bayes import GaussianNB
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.preprocessing import LabelEncoder

import symptomchecker_pb2
import symptomchecker_pb2_grpc


_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class Greeter(symptomchecker_pb2_grpc.GreeterServicer):

  def SendFeatureLabel(self, request, context):
    #return symptomchecker_pb2.StringReply(predict=request.fdata)
    feat=[]
    dt=[]
    for i in range(len(request.feature)):
        feat.append(request.feature[i].fdata)	
	        # mlfeat.append(request.feature)
   		 		
    for k in range(len(request.data)):	 
	    dt.append(request.data[k].pdata) 
    print('\n')
	
    print(request.feature)	
    print(request.data)
    presult=Greeter.MlCalculate(feat,request.label,dt) 
    return symptomchecker_pb2.StringReply(predict = presult )
		

		
  def MlCalculate(mlfeat,mlabel,data):		
    mlb=MultiLabelBinarizer()
    le=LabelEncoder()
    t_features=	mlb.fit(mlfeat)
    f_class=mlb.classes_
    print(f_class)
    t_features = mlb.transform(mlfeat)
    print(t_features)
    t_labels =le.fit_transform(mlabel)
    print("\n")
    print(t_labels)
    clf = GaussianNB()
    clf = clf.fit(t_features,t_labels)
	
    t_data = mlb.fit_transform(data)
    print("\n ",t_data)
    print(clf.predict([t_data[1]]))
    predicted = le.inverse_transform(clf.predict([t_data[1]]))
    return predicted[0]
	
    #return symptomchecker_pb2.StringReply(predict=request.feature[1].fdata)
	
  #def SendArrayAgain(self, request, context):
      #return symptomchecker_pb2.StringReply(predict=request.fdata)	
		

def serve():
  server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
  symptomchecker_pb2_grpc.add_GreeterServicer_to_server(Greeter(), server)
  server.add_insecure_port('[::]:50051')
  server.start()
  try:
    while True:
      time.sleep(_ONE_DAY_IN_SECONDS)
  except KeyboardInterrupt:
    server.stop(0)

if __name__ == '__main__':
  serve()
